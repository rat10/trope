# trope

**trope** aims to facilitate webbased visualizatios for Tor metrics data. It is part of the [**trice**](https://gitlab.com/rat10/trice) analytics suite for Tor network metrics data.

The [Tor](https://www.torproject.org/index.html) project develops a network that provides users with anonymity while on the internet. 
It adds to this some means for censorship circumvention as adversaries try to block access to Tor alltogether. 
The Tor infrastructure is comprised of several types of network nodes, and a lot thereof.

Visualizing all the parts of this network in a meaningful way is propably not possible but of course insights can be drawn from combining different aspects and sources in one view.
**trope** builds on the [visionion](http://www.github.com/tomlurge/visionion) project which got bogged down by the complexity of the data structures and upfront aggregation tasks and has been abandoned before getting any useful visualization off the ground.
Visionion aimed to integrate and visualize all available data in a generic and easily extensible fashion.
One should then have been able to combine and tailor these generic views to elucidate structural patterns and hidden aspects in the data.

**trope** will rather explore how generic visualization tools like Apache [Zeppelin](https://zeppelin.apache.org/) can be put to good use. Only then, maybe, will the original poject of a complex standalone Tor network visualization be revived.

