# visionion

visionion aims to provide a webbased visualization tool for Tor metrics data.

The [Tor](https://www.torproject.org/index.html) project is primarily a system to provide a user with anonymity while on the internet.
It adds to this some means for censorship circumvention as adversaries try to block access to Tor alltogether.
The Tor infrastructure is comprised of several types of network nodes, and a lot thereof.

Visualizing all the parts of this network in a meaningful way is propably not possible but of course insights can be drawn from combining different aspects and sources in one view.
Visionion aims to integrate and visualize all available data in a generic and easily extensible fashion.
These generic views can then be combined and tailored to elucidate structural patterns and hidden aspects in the data.


* [Technical Overview](technicalOverview.md) 
	* [Database](technicalOverview.md#database) 
	* [Web Application Framework](technicalOverview.md#webapp) 
	* [Visualization Framework](technicalOverview.md#vis) 
* [Visualization](visualization.md)  
* [How To scaffold the web application](how2webAppFramework.md)
* [TODO](todo.md)

-> everything below has been moved to [**treat**](https://gitlab.com/rat10/treat/doc/visionion)
* [Aggregation](aggregation.md)  
	* [Import Data Aggregation](aggregation.md#import)  
		* [Structural Overview](aggregation.md#structure)  
			* [Projective versus Disjunctive](aggregation.md#prodis)  
		* [Reports](aggregation.md#reports)  
			* [Client Report](aggregation.md#client)  
			* [Server Reports](aggregation.md#server)  
			* [Country Reports](aggregation.md#country)  
			* [Autonomous System Reports](aggregation.md#as)  
		* [Future Possibilities](aggregation.md#future)  
		* [Structure Detailed](aggregation.md#detailed)  
		* [Annotated + Radically Shortened Sample Fact](annotatedFactRow.md)
	* [Consolidation and Simplification](aggregation.md#consolidate)  
	* [Aggregation Admin Interface](aggregation.md#admin)  
	* [MapReduce with MongoDB](aggregation.md#mongoDB)   
	* [Notes](aggregation.md#notes)  
* [Technicalities](technicalities.md)  
	* [Background Information](technicalities.md#background)
	* [JavaScript Issues](technicalities.md#jsIssues)   
	* [Indexing](technicalities.md#indexing)   
		* [Indexing Import Data](technicalities.md#indexingImport) 
		* [Indexing Aggregated Data](technicalities.md#indexingAggregated)   
	* [Working Notes](technicalities.md#notes)  
* [Acronyms](acronyms.md)
